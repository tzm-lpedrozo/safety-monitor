package br.com.tzm.safety.controladores;

import br.com.tzm.safety.entidades.Equipamento;
import br.com.tzm.safety.servicos.EquipamentoService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

// Aqui dizemos que este é um controlador REST (implementa um serviço RESTful de comunicação)
@RestController
// Aqui mapeamos o endereço que queremos acessar os recursos dessa API (no caso, ex.: http://localhost:8080/equipamentos)
@RequestMapping("/equipamentos")
public class EquipamentoController extends EntidadeController<Equipamento, EquipamentoService> {

	// Todos os outros métodos (criar, lista, buscar, salvar, excluir) são herdados do EntidadeController
	// e seus mapeamentos vêm com eles

}
