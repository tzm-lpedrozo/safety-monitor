package br.com.tzm.safety.servicos;

import br.com.tzm.safety.entidades.Entidade;
import br.com.tzm.safety.repositorios.EntidadeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public abstract class EntidadeService<T extends Entidade, R extends EntidadeRepository<T>> {

	// Todos os métodos dessa classe implementam operações básicas de banco de dados
	// Outros services podem especializar e adicionar recursos de processamento, conforme as entidades gerenciadas

	// Declara para o Spring que o objeto deve ser injetado automaticamente
	@Autowired
	// Esse é nosso repositório, que fará as operações de banco de dados
	protected R repository;

	public T criar() {
		return repository.criar();
	}

	// Declara que o método abaixo deve ter mecanismo de transação (commit, rollback) automático
	@Transactional
	public T salvar(T entidade) {
		return repository.salvar(entidade);
	}

	// Declara que o método abaixo deve ter mecanismo de transação (commit, rollback) automático
	@Transactional
	public void excluir(Long id) {
		repository.excluir(id);
	}

	public T buscar(Long id) {
		return repository.buscar(id);
	}

	public List<T> listar() {
		return repository.listar();
	}

}
