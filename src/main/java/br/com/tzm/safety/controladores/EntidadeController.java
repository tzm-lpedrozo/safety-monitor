package br.com.tzm.safety.controladores;

import br.com.tzm.safety.entidades.Entidade;
import br.com.tzm.safety.servicos.EntidadeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

public abstract class EntidadeController<T extends Entidade, S extends EntidadeService<T, ?>> {

	// O controller é o responsável pelo mapeamento dos métodos da API do backend
	// É ele que conversa com o frontend, entregando os dados para montagem da interface

	// Declara para o Spring que o objeto deve ser injetado automaticamente
	@Autowired
	// Esse é nosso serviço, que dispõe as informações que precisamos apresentar para o frontend
	protected S service;

	// Cria o mapeamento de método HTTP GET para o endereço "/new" (ex.: GET http://localhost:8080/entidade/new)
	@GetMapping("/new")
	public T criar() {
		return service.criar();
	}

	// Cria o mapeamento de método HTTP GET para o endereço com ID (ex.: GET http://localhost:8080/entidade/1)
	@GetMapping("/{id}")
	public T buscar(@PathVariable Long id) {
		return service.buscar(id);
	}

	// Cria o mapeamento de método HTTP DELETE para o endereço com ID (ex.: DELETE http://localhost:8080/entidade/1)
	@DeleteMapping("/{id}")
	public void excluir(@PathVariable Long id) {
		service.excluir(id);
	}

	// Cria o mapeamento de método HTTP POST para o endereço (ex.: POST http://localhost:8080/entidade)
	// A entidade a ser salva fica no corpo (body) da requisição
	@PostMapping
	public T salvar(@RequestBody T entidade) {
		return service.salvar(entidade);
	}

	// Cria o mapeamento de método HTTP GET para o endereço (ex.: http://localhost:8080/entidade)
	// Este método traz todas as entidades da tabela
	@GetMapping
	public List<T> listar() {
		return service.listar();
	}

}
