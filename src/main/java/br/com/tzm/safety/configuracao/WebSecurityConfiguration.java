package br.com.tzm.safety.configuracao;

import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

// Habilita o mecanismo de segurança do backend (necessário para que o frontend aceite a conexão)
@EnableWebSecurity
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		// Para ligar autenticação, basta trocar "anonymous" por "authenticated"
		http.authorizeRequests().anyRequest().anonymous().and().cors().and().csrf().disable();
	}

}
