package br.com.tzm.safety.repositorios;

import br.com.tzm.safety.entidades.Entidade;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.lang.reflect.ParameterizedType;
import java.util.List;

public abstract class EntidadeRepository<T extends Entidade> {

	// Essa será nossa referência para a classe de entidade que será gerenciada por este repositório
	private Class<T> entityClass;

	// Declara este entityManager como pertencendo ao contexto de persistência (banco de dados)
	@PersistenceContext
	// O entityManager é o reponsável por executar os comandos de banco de dados (SQLs)
	protected EntityManager entityManager;

	@SuppressWarnings("unchecked")
	public EntidadeRepository() {
		// Obtém qual a classe que especializa nosso repositório genérico
		this.entityClass = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
	}

	public T criar() {
		try {
			// Retorna uma nova instância do objeto gerenciado por este repositório
			return entityClass.getConstructor().newInstance();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public void excluir(Long id) {
		// Exclui o objeto, usando o ID como filtro
		entityManager.remove(entityManager.find(entityClass, id));
	}

	public T salvar(T entidade) {
		// Caso o ID seja nulo, precisamos primeiro persistir (insert) no banco de dados
		if (entidade.getId() == null) {
			// Insere no banco de dados
			entityManager.persist(entidade);
		} else {
			// Caso contrário, precisamos só atualizar (update) o registro
			entidade = entityManager.merge(entidade);
		}
		return entidade;
	}

	public T buscar(Long id) {
		// Busca (select) o objeto do banco de dados, usando o ID como filtro (select * from tabela where id = ?)
		return entityManager.find(entityClass, id);
	}

	public List<T> listar() {
		// Obtém uma lista completa (select * from tabela), sem filtro
		return entityManager.createQuery("from " + entityClass.getName(), entityClass).getResultList();
	}

}
