package br.com.tzm.safety.entidades;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.Transient;

@Data
// Diz ao Hibernate que essa classe representa uma tabela no banco de dados
// Assim, ele cria pra nós (create table) essa tabela com todos os membros (campos) que declaramos (exceto os anotados com @Transient)
@Entity
public class Equipamento extends Entidade {

	private String descricao;

	private String ip;

	// Diz ao Hibernate que ele NÃO deve salvar os dados deste campo na tabela
	@Transient
	private Double falha;

}
