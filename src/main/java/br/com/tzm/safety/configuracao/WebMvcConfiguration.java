package br.com.tzm.safety.configuracao;

import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

// Habilita o mecanismo de segurança do backend (necessário para que o frontend aceite a conexão)
@EnableWebSecurity
public class WebMvcConfiguration implements WebMvcConfigurer {

	// Estes são os métodos HTTP que estamos usando para manipular os dados
	private static final String[] ALLOWED_METHODS = {"GET", "POST", "DELETE"};

	@Override
	public void addCorsMappings(CorsRegistry registry) {
		// Cria o mapeamento do CORS (recurso que o navegador usa para garantir a segurança da conexão com o backend)
		registry.addMapping("/**").allowedOrigins("*").allowCredentials(true).allowedMethods(ALLOWED_METHODS);
	}

}
