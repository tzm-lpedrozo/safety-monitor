package br.com.tzm.safety;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

// Habilita o mecanismo de auto-agendamento (usado no EquipamentoService para pings de equipamentos)
@EnableScheduling
// Declara a classe que será a raiz da aplicação do Spring
@SpringBootApplication
public class Main {

	public static void main(String[] args) {
		SpringApplication.run(Main.class, args);
	}

}
