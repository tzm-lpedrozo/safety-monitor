package br.com.tzm.safety.entidades;

import lombok.Data;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@Data
// Declara que esta classe é um modelo abstrato de entidade, a ser especializada por outras
@MappedSuperclass
public abstract class Entidade {

	// Diz ao Hibernate que este é o campo que será usado como chave primária (PK)
	@Id
	// Diz ao Hibernate que esta chave deve ser gerada automaticamente
	@GeneratedValue
	private Long id;

}
