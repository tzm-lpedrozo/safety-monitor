package br.com.tzm.safety.repositorios;

import br.com.tzm.safety.entidades.Equipamento;
import org.springframework.stereotype.Repository;

// Diz ao Spring que esse é um repositório (que provavelmente será usado por um serviço)
@Repository
public class EquipamentoRepository extends EntidadeRepository<Equipamento> {

	// Tudo que declaramos no EntidadeRepository é herdado aqui, então temos acesso às operações
	// de banco de dados para nossa tabela de equipamentos

}
