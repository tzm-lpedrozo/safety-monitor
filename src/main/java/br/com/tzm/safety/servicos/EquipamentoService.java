package br.com.tzm.safety.servicos;

import br.com.tzm.safety.entidades.Equipamento;
import br.com.tzm.safety.repositorios.EquipamentoRepository;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

// Diz ao Spring que esse é um serviço que deve ser criado automaticamente. Ele será usado por um controller ou por outros componentes do sistema.
@Service
public class EquipamentoService extends EntidadeService<Equipamento, EquipamentoRepository> {

	// Contador de pings
	private int pings;

	// Mapa de falhas por equipamento (mapeado usando o ID do equipamento)
	private Map<Long, Integer> falhas = new ConcurrentHashMap<>();

	@Override
	public List<Equipamento> listar() {
		// Obtém a lista de equipamentos do banco de dados
		List<Equipamento> equipamentos = super.listar();
		// Para cada equipamento, copia o percentual de falhas (divisão da quantidade de pings pelo número de falhas do equipamento)
		equipamentos.forEach(equipamento -> equipamento.setFalha(Double.valueOf(Optional.ofNullable(falhas.get(equipamento.getId())).orElse(0)) / pings));
		// Retorna a lista já com as falhas preenchidas para o frontend
		return equipamentos;
	}

	// Executa depois da inicialização
	@PostConstruct
	// Agendamento para execução automática a cada 30 segundos, todos os dias
	@Scheduled(cron = "0/30 * * * * ?")
	public void pingar() {
		// Incrementa o contador de pings
		++pings;
		// Para cada equipamento (listado no banco de dados) tenta fazer o ping
		super.listar().forEach(equipamento -> {
			try {
				if (!InetAddress.getByName(equipamento.getIp()).isReachable(5000)) {
					// Caso o ping falhe, incrementa o contador de falhas (criando do zero, caso não exista)
					falhas.put(equipamento.getId(), Optional.ofNullable(falhas.get(equipamento.getId())).orElse(0) + 1);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		});
	}

}
